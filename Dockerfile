FROM ruby:2.3-alpine

WORKDIR /var/bots/

COPY Gemfile /var/bots/

RUN apk --no-cache add --update --virtual ruby_dep \
        build-base \
        ruby-dev \
    && bundle install \
    && rm -rf ~/.bundle \
    && apk --no-cache del ruby_dep

RUN apk --no-cache add --update \
        git \
    && mkdir /tmp/sounds-repo \
    && git -C /tmp/sounds-repo init \
    && git -C /tmp/sounds-repo remote add -f origin https://github.com/2ec0b4/kaamelott-soundboard/ \
    && git -C /tmp/sounds-repo config core.sparseCheckout true \
    && echo "sounds" > /tmp/sounds-repo/.git/info/sparse-checkout \
    && git -C /tmp/sounds-repo pull --depth=1 origin master \
    && mv /tmp/sounds-repo/sounds /var/bots/sounds \
    && rm -rf /tmp/sounds-repo \
    && apk --no-cache del git \
    && rm -rf /var/cache/apk

COPY ./src/ /var/bots/

CMD ruby /var/bots/main.rb

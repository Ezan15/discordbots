#!/usr/bin/ruby

class Thanks
    def initialize thanksPath
        @thanksAnswers = JSON.parse(File.read(thanksPath))
        @thanksRegex =
            /\b(merci|nice|bien joué|cool|parfait)\b \bdip(lodocus)?\b/i
    end
    def reactTo(event)
        return @thanksAnswers.sample if @thanksRegex.match event.content
    end
end
